package com.logicinfo.extra.store;

public class InActiveTrackingOrders {

	private String CustomerOrderNo;

	private String trackingId;

	private Integer carrierId;

	private String processInd;

	private Integer deliveryId;

	private String carrierCode;

	public InActiveTrackingOrders() {

	}

	public InActiveTrackingOrders(String customerOrderNo, String trackingId, Integer carrierId, String processInd,
			Integer deliveryId, String carrierCode) {
		super();
		CustomerOrderNo = customerOrderNo;
		this.trackingId = trackingId;
		this.carrierId = carrierId;
		this.processInd = processInd;
		this.deliveryId = deliveryId;
		this.carrierCode = carrierCode;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getCustomerOrderNo() {
		return CustomerOrderNo;
	}

	public void setCustomerOrderNo(String customerOrderNo) {
		CustomerOrderNo = customerOrderNo;
	}

	public String getTrackingId() {
		return trackingId;
	}

	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}

	public Integer getCarrierId() {
		return carrierId;
	}

	public void setCarrierId(Integer carrierId) {
		this.carrierId = carrierId;
	}

	public String getProcessInd() {
		return processInd;
	}

	public void setProcessInd(String processInd) {
		this.processInd = processInd;
	}

	public Integer getDeliveryId() {
		return deliveryId;
	}

	public void setDeliveryId(Integer deliveryId) {
		this.deliveryId = deliveryId;
	}

	@Override
	public String toString() {
		return "InActiveTrackingOrders [CustomerOrderNo=" + CustomerOrderNo + ", trackingId=" + trackingId
				+ ", carrierId=" + carrierId + ", processInd=" + processInd + ", deliveryId=" + deliveryId
				+ ", carrierCode=" + carrierCode + "]";
	}

}
