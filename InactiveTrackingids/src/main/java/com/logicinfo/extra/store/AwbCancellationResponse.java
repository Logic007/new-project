package com.logicinfo.extra.store;

public class AwbCancellationResponse {

	private String statusCode;
	private String statusMessage;

	public AwbCancellationResponse() {

	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

}
